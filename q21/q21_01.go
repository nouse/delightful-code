package q21

import (
	"math/big"
	"math/bits"
)

func layerOfZeroTriangle(n uint) int {
	layer := 0
	remain := int(n)
	number := big.NewInt(1)
	nextNumber := new(big.Int)
	for remain > 0 {
		layer++
		z := zeroes(number)
		remain -= z
		nextNumber.Lsh(number, 1)
		number.Xor(number, nextNumber)
	}
	return layer
}

func zeroes(n *big.Int) int {
	m := 0
	for i, w := range n.Bytes() {
		if i == 0 {
			m += bits.Len8(w)
		} else {
			m += 8
		}
		m -= bits.OnesCount8(w)
	}
	return m
}
