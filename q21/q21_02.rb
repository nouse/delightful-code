# frozen_string_literal: true

def layers_of_zero(n)
  layer = 0
  remain = n
  number = 1
  while remain > 0
    layer += 1
    remain -= number.to_s(2).count("0")
    number ^= number << 1
  end
  layer
end

require 'benchmark/ips'

Benchmark.ips do |x|
  x.report("layer of zero 2014"){ layers_of_zero(2014)}
end
