package q21

import (
	"testing"
)

func TestLayerOfZero(t *testing.T) {
	t.Log(layerOfZeroTriangle(2014))
}

func BenchmarkLayerOfZero(b *testing.B) {
	for i := 0; i < b.N; i++ {
		layerOfZeroTriangle(2014)
	}
}
