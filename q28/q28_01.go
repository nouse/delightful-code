package q28

import "sort"

// Club is struct for different clubs
type Club struct {
	area   uint
	people uint
}

func max(clubs []Club, total uint) uint {
	s := sum(clubs)

	if s.people < total {
		return s.area
	}

	// sort with reverse order of people count
	sort.Slice(clubs, func(i, j int) bool {
		return clubs[i].area*clubs[j].people > clubs[i].people*clubs[j].area
	})

	return max2(clubs, total, s)
}

// clubs is sorted
func max2(clubs []Club, total uint, sum Club) uint {

	if sum.people < total {
		return sum.area
	}

	if len(clubs) == 0 {
		return 0
	}

	if clubs[0].people > total {
		return max2(clubs[1:], total, sum)
	}

	newSum := Club{
		area:   sum.area - clubs[0].area,
		people: sum.people - clubs[0].people,
	}
	cand0 := clubs[0].area + max2(clubs[1:], total-clubs[0].people, newSum)
	cand1 := max2(clubs[1:], total, sum)

	if cand0 > cand1 {
		return cand0
	}
	return cand1
}

func sum(clubs []Club) Club {
	var (
		sum, areas uint
	)

	for _, c := range clubs {
		areas += c.area
		sum += c.people
	}
	return Club{areas, sum}
}
