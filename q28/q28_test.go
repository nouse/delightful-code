package q28

import "testing"

func TestMax(t *testing.T) {
	clubs := []Club{
		{11000, 40},
		{8000, 30},
		{400, 24},
		{800, 20},
		{900, 14},
		{1800, 16},
		{1000, 15},
		{7000, 40},
		{100, 10},
		{300, 12},
	}
	if max(clubs, 150) != 28800 {
		t.Errorf("max of clubs should be 28800, actual: %d", max(clubs, 150))
	}
}

func BenchmarkMax(b *testing.B) {
	clubs := []Club{
		{11000, 40},
		{8000, 30},
		{400, 24},
		{800, 20},
		{900, 14},
		{1800, 16},
		{1000, 15},
		{7000, 40},
		{100, 10},
		{300, 12},
	}
	for i := 0; i < b.N; i++ {
		newClubs := make([]Club, len(clubs))
		copy(newClubs, clubs)
		max(clubs, 150)
	}
}
