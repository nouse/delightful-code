package q01

import (
	"testing"
)

func BenchmarkReverseString(b *testing.B) {
	for i := 0; i < b.N; i++ {
		PalindromeNumber()
	}
}

func BenchmarkReverseInteger(b *testing.B) {
	for i := 0; i < b.N; i++ {
		PalindromeNumber2()
	}
}

func TestPalindromeNumber2(t *testing.T) {
	num := PalindromeNumber2()
	if num != 585 {
		t.Errorf("Expected PalindromeNumber2 to return 585, actual %d", num)
	}
}
