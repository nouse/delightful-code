from q01_03 import palindrome_number2
import palindrome as p


def test_answer():
    assert p.palindrome_number() == 585
    assert palindrome_number2() == 585


def test_palindrom_cython(benchmark):
    benchmark(p.palindrome_number)


def test_palindrom_int(benchmark):
    benchmark(palindrome_number2)


