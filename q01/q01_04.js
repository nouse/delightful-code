'use strict'
const CANDIDATES = [
    11, 33, 55, 77, 99,
    101, 111, 121, 131, 141, 151, 161, 171, 181, 191,
    303, 313, 323, 333, 343, 353, 363, 373, 383, 393,
    505, 515, 525, 535, 545, 555, 565, 575, 585, 595,
    707, 717, 727, 737, 747, 757, 767, 777, 787, 797,
    909, 919, 929, 939, 949, 959, 969, 979, 989, 999,
]

function is_palindrome(s) {
    var ls = s.length
    for (var i = 0; i < ls / 2; i++) {
        if (s[i] != s[ls - 1 - i]) {
            return false
        }
    }
    return true
}

function palindrome_number() {
    for (var num of CANDIDATES) {
        if (is_palindrome(num.toString(2)) && is_palindrome(num.toString(8))) {
            return num
        }
    }
    return -1
}

exports.palindrome_number = palindrome_number

function reverse8(num) {
    var result = num & 7
    var remain = num >> 3
    while (remain > 0) {
        result = (result << 3) + (remain & 7)
        remain >>= 3
    }
    return result
}

const reverseTable = [0, 4, 2, 6, 1, 5, 3, 7]
const movTable = [0, 1, 2, 2, 3, 3, 3, 3]
const remTable = [0, 1, 1, 3, 1, 5, 3, 7]

function reverse2(num) {
    var result = reverseTable[num & 7]
    var remain = num >> 3
    while (remain > 7) {
        result = (result << 3) + reverseTable[remain & 7]
        remain >>= 3
    }
    if (remain == 0) {
        return result
    }
    return (result << movTable[remain]) + remTable[remain]
}

function palindrome_number2() {
    for (var num of CANDIDATES) {
        if (reverse8(num) == num && reverse2(num) == num) {
            return num
        }
    }
    return -1
}

exports.palindrome_number2 = palindrome_number2