'use strict'
const p = require("./q01_04")

describe('palindrome', () => {
    it("should return 585", () => {
        is(p.palindrome_number(), 585)
        is(p.palindrome_number2(), 585)
    })
})
bench("bench", () => {
    it("palindrom string", () => {
        p.palindrome_number()
    })
    it("palindrom int", () => {
        p.palindrome_number2()
    })
})
