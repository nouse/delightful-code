package q02

func find() int {
	for i := 1000; i < 10000; i++ {
		if validate(i) {
			return i
		}
	}
	return -1
}

func validate(num int) bool {
	reverse, digits := digits(num)
	return checkMultiples(reverse, digits)
}

func number(nums []int) int {
	if len(nums) == 1 {
		return nums[0]
	}
	i := 0
	for _, num := range nums {
		i = (i * 10) + num
	}
	return i
}

func checkMultiples(target int, nums []int) bool {
	length := len(nums)
	if length == 1 {
		return nums[0] == target
	}
	if length == 2 {
		return number(nums) == target || (nums[0]*nums[1]) == target
	}

	for i := 1; i < length-1; i++ {
		number := number(nums[0:i])
		if number != 0 && target%number == 0 && checkMultiples(target/number, nums[i:length]) {
			return true
		}
	}
	return false
}

func digits(num int) (int, []int) {
	remain := num / 10
	mod := num % 10
	reverse := []int{mod}
	for remain > 0 {
		mod := remain % 10
		reverse = append(reverse, mod)
		remain /= 10
	}

	length := len(reverse)
	final := make([]int, length)
	for i := 0; i < length; i++ {
		final[i] = reverse[length-1-i]
	}

	return number(reverse), final
}
