def find
  (1000..9999).each do |i| 
    if validate(i)
      return i
    end
  end
  nil
end

def validate(i)
  reverse, digits = digits(i)
  multiples(digits).include?(reverse)
end

def number(array)
  if array.length == 1
    return array[0]
  end
  return array.inject{|m, i| m*10 +i}
end

def multiples(array)
  len = array.length
  if len == 1
    return array
  end

  if len == 2
    return [number(array), array[0]*array[1]]
  end
  result = []
  (1..len-1).each do |i| 
    multiples(array[i..-1]).each do |m| 
      result << number(array[0...i])*m
    end
  end
  result
end

def digits(i)
  remain, mod = i.divmod 10
  result = [mod]
  while remain > 10
    remain, mod = remain.divmod 10
    result << mod
  end
  result << remain
  return number(result), result.reverse!
end

if $0 == __FILE__
  require 'benchmark/ips'
  Benchmark.ips do |x|
    x.report("find magic number"){ find }
  end
end
