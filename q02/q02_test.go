package q02

import (
	"testing"
)

func TestFind(t *testing.T) {
	num := find()
	if num != 5931 {
		t.Errorf("Expected find() to return 5931, actual %d", num)
	}
}

func BenchmarkFind(b *testing.B) {
	for i := 0; i < b.N; i++ {
		find()
	}
}

func TestCheckMultiples(t *testing.T) {
	if !checkMultiples(1395, []int{5, 9, 3, 1}) {
		t.Error("checkMultiples 1395 and 5,9,3,1 should return true")
	}
}
